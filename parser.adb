with Ada.Text_IO, Ada.Characters.Handling, Linear_Search;
use Ada.Text_IO, Ada.Characters.Handling;

package body Parser is
	Parser_Error : exception;
	
	function Length(St: State) return Natural is
	begin
		return St.Size;
	end Length;
	
	procedure Put(Exp: Expression) is
	begin
		if Exp.Op = PRN then
			Put(Exp.Op'Image & "(" & Exp.LHS'Image & ")");
		else
			Put(Exp.Op'Image & "(" & Exp.LHS'Image & ", " & Exp.RHS'Image & ")");
		end if;
	end Put;
	
	procedure Put(St: State) is
	begin
		for I in 1..St.Size loop
			Put(St.Expressions(I));
			New_Line;
		end loop;
	end Put;
	
	procedure Compute(St: State; Vars: in out Variable_Array) is
		E: Expression;
	begin
		for I in 1..St.Size loop
			E:=St.Expressions(I);
			Put(E);
			New_Line;
			case E.Op is
				when ADD => Vars(E.LHS) := Vars(E.LHS) + Vars(E.RHS);
				when SUB => Vars(E.LHS) := Vars(E.LHS) - Vars(E.RHS);
				when MUL => Vars(E.LHS) := Vars(E.LHS) * Vars(E.RHS);
				when PRN =>
					Put(Vars(E.LHS));
					New_Line;
				when INI =>
					if E.RHS = '0' then
						Vars(E.LHS) := Default_Value;
					else
						Vars(E.LHS) := Vars(E.RHS);
					end if;
			end case;
		end loop;
	end Compute;
	
	procedure Parse(Src: String; St: in out State) is
		Len : Natural := Src'Length;
		
		function Char_NEq(A,B: Character) return Boolean is
		begin
			return A/=B;
		end Char_NEq;
		
		--subtype StrInd is String'Range;
		
		procedure Linear_Search_Other_Char is new Linear_Search(
			Item => Character,
			-- type String is array(Positive range <>) of Character;
			-- https://www.adaic.org/resources/add_content/standards/05rm/html/RM-3-6-3.html
			Index => Positive,
			Item_Array => String,
			Predicate => Char_NEq
		);
				
		Found: Boolean;
		Found_At: Integer;
		Parsed_Until: Positive;
		
		function Parse_Op(S:String) return Operator is
		begin
			for Op in Operator'Range loop
				if Op'Image = S then
					return Op;
				end if;
			end loop;
			raise Parser_Error;
		end Parse_Op;
	begin
		Parsed_Until := Src'First;
		-- skip leading whitespace
		Linear_Search_Other_Char(Src, ' ', Found, Found_At);
		if Found then
			Parsed_Until := Found_At;
		end if;
		while Parsed_Until < Src'Last loop
			declare
				Op : Operator;
				L : Character;
				R : Character;
			begin
				Op := Parse_Op(Src(Parsed_Until..Parsed_Until+2));
				Parsed_Until := Parsed_Until + 2;
				
				-- parse LHS
				Linear_Search_Other_Char(Src(Parsed_Until+1..Src'Last), ' ', Found, Found_At);
				if Found then
					Parsed_Until := Found_At;
					L := Src(Found_At);
				else
					raise Parser_Error;
				end if;
				
				-- parse RHS only if operator needs it
				if Op /= PRN then
					Linear_Search_Other_Char(Src(Parsed_Until+1..Src'Last), ' ', Found, Found_At);
					if Found then
						Parsed_Until := Found_At;
						R := Src(Found_At);
					else
						raise Parser_Error;
					end if;
				end if;
				
				St.Size := St.Size + 1;
				St.Expressions(St.Size) := (Op, L, R);
				
				-- find semicolon
				Linear_Search_Other_Char(Src(Parsed_Until+1..Src'Last), ' ', Found, Found_At);
				if Found then
					Parsed_Until := Found_At;
					if Src(Found_At) /= ';' then
						raise Parser_Error;
					end if;
				else
					raise Parser_Error;
				end if;
				
				-- skip trialing whitespace
				if Parsed_Until < Src'Last then
					Linear_Search_Other_Char(Src(Parsed_Until+1..Src'Last), ' ', Found, Found_At);
					if Found then
						Parsed_Until := Found_At;
					else
						Parsed_Until := Src'Last;
					end if;
				end if;
			end;
		end loop;
	end Parse;
end Parser;
