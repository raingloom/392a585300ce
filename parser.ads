generic
	type Value is private;
	type Variable_Array is array (Character range <>) of Value;
	Default_Value : Value;
	with function "+" (A,B: Value) return Value is <>;
	with function "-" (A,B: Value) return Value is <>;
	with function "*" (A,B: Value) return Value is <>;
	with procedure Put(A: Value);
package Parser is
	type State(Capacity: Natural) is private;
	type Expression is private;
	type Operator is (ADD, SUB, MUL, PRN, INI);
	procedure Parse(Src: String; St: in out State);
	procedure Compute(St: State; Vars: in out Variable_Array);
	procedure Put(Exp: Expression);
	procedure Put(St: State);
	function Length(St: State) return Natural;
private
	type Expression_Array is array (Positive range <>) of Expression;
	type State(Capacity: Natural) is record
		Expressions: Expression_Array(1..Capacity);
		Size: Natural := 0;
	end record;
	
	type Expression is
	record
		Op : Operator;
		LHS : Character;
		RHS : Character;
	end record;
end Parser;
