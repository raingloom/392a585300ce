	procedure Linear_Search(Items: in Item_Array; Target_Item: in Item; Found: out Boolean; Found_At: out Index) is
	begin
		for I in Items'Range loop
			if Predicate(Items(I), Target_Item) then
				Found := True;
				Found_At := I;
				return;
			end if;
		end loop;
		Found := False;
	end Linear_Search;
