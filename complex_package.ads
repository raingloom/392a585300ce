generic
	type Value is private;
	with function "+"(A,B:Value) return Value is <>;
	with function "-"(A,B:Value) return Value is <>;
	with function "*"(A,B:Value) return Value is <>;
	with procedure Put(A:Value) is <>;
package Complex_Package is
	type Complex is record
		Re,Im: Value;
	end record;
	function "+" (A,B:Complex) return Complex;
	function "-" (A,B:Complex) return Complex;
	function "*" (A,B:Complex) return Complex;
	procedure Put(A:Complex);
end Complex_Package;
