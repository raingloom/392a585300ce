with Ada.Text_IO;
use Ada.Text_IO;

package body Complex_Package is
	function "+"(A,B : in Complex) return Complex is
	begin
		return (A.Re+B.Re, A.Im+B.Im);
	end;
	
	function "-"(A,B : in Complex) return Complex is
	begin
		return (A.Re-B.Re, A.Im-B.Im);
	end;
	
	function "*"(A,B : in Complex) return Complex is
	begin
		-- (a+bi)*(x+yi)=(ax+ayi+xbi-yb)=()
		return (A.Re * B.Re - A.Im * B.Im,
			A.Im * B.Re + A.Re * B.Im);
	end;
	
	procedure Put(A : in Complex) is
	begin
		Put("("); Put(A.Re); Put("+"); Put(A.Im); Put("*i )");
	end Put;
end Complex_Package;
