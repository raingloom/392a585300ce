with Ada.Text_IO, Ada.Integer_Text_IO, Ada.Float_Text_IO, Parser, Complex_Package;
use Ada.Text_IO;

procedure Parser_Test is
begin
	--  Integer test
	declare
		type VArr is array (Character range <>) of Integer;
		procedure IPut(I:Integer) is
		begin
			Ada.Integer_Text_IO.Put(I);
		end IPut;
		package Prs is new Parser(Value=>Integer, Variable_Array=>VArr, Default_Value=>0, Put=>IPut);
		use Prs;
		St : Prs.State(100);
		Vars : VArr('a'..'c') := (5,3,8);
	begin
		Parse("MUL a b;INI b 0;ADD c b;INI b c;PRN b;", St);
		Put(St);
		Compute(St, Vars);
		if Vars=(15,8,8) then
			Put("Integer test passed");
			New_Line;
		end if;
	end;
	
	-- Complex float test
	declare
		procedure CPut(X:Float) is
		begin
			Ada.Float_Text_IO.Put(X);
		end;
		package CP is new Complex_Package(Value=>Float, Put=>CPut);
		use CP;
		type VArr is array (Character range <>) of Complex;
		package Prs is new Parser(Value=>Complex, Variable_Array=>VArr, Default_Value=>(0.0,0.0), Put=>CP.Put);
		use Prs;
		St : Prs.State(100);
		Vars : VArr('a'..'c') := ((1.0,2.0),(0.5,1.0),(3.1,9.9));
	begin
		Parse("ADD a b;INI c a; SUB a c;MUL b a;", St);
		Put(St);
		Compute(St, Vars);
		if Vars=((0.0,0.0),(0.0,0.0),(1.5,3.0)) then
			Put("Complex float test passed");
			New_Line;
		end if;
	end;
end Parser_Test;
