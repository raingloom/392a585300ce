generic
	type Item is private;
	type Index is (<>);
	with function Predicate(A,B: in Item) return Boolean;
	type Item_Array is array(Index range <>) of Item;
procedure Linear_Search(Items: in Item_Array;
	Target_Item: in Item;
	Found: out Boolean;
	Found_At: out Index);
